# Chat Backend

Das Chat Backend ist ein auf MYSQL und NodeJS basiertes Backend für Chatanwendungen.

Die Benutzung hägt von den Voraussetzungen ab:

## DB ist vorhanden und Start ohne Docker

```bash
$ npm install or yarn install  #This will install all dependencies

Edit the .env File for your DB Data

#run the Server local
$ npm start
```

## DB ist vorhanden und Start mit Docker

```bash
$ npm install or yarn install  #This will install all dependencies

Edit the .env File for your DB Data

#build and run Dockerfile
$ docker-compose.server.yml
```

## Start mit Docker

```bash
$ npm install or yarn install  #This will install all dependencies

#build and run Dockerfile
$ docker-compose.yml
```
