WORKDIR /usr/src/app

RUN apk update && apk add --no-cache make gcc g++ python linux-headers udev

COPY package.json .
COPY ./dist ./dist

RUN npm install --only=production

EXPOSE 3000

CMD ["npm", "run", "start:production"]
