import Server from "./Server";
import {connect} from "./Sequelize";

//start Server on Port 3000
const server: any = new Server();

//startServer is an async function, so we must handle the response with a simple done
server.startServer().done();

//connect to DB via Sequelize
connect();
