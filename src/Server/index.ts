import "reflect-metadata";
import {buildSchema} from "type-graphql";
import express from 'express';
import * as path from "path";
import graphqlHTTP from 'express-graphql';
import {SERVER_PORT} from "../config";

export default class Server {

    //create an async public function to Start the server
    public async startServer() {

        //build DB and GraphQL Schema
        //because of await we need the async function
        const schema = await buildSchema({
            resolvers: [],
            emitSchemaFile: path.resolve(__dirname, "schema.gql"),
        });

        //instantiate express Server
        const app = express();

        //create an /graphQL Route for testing
        app.use(
            '/graphql',
            graphqlHTTP({
                schema: schema,
                graphiql: true
            })
        );

        //start express Server and listen on PORT
        app.listen(SERVER_PORT, function () {
            console.log('Example app listening on port 3000!');
        });
    };

}
