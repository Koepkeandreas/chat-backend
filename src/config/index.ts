const env = process.env;

//initialize MySql Data from .env File
export const MYSQL_ROOT_PASSWORD: string = env.MYSQL_ROOT_PASSWORD || 'pwd';
export const MYSQL_DATABASE: string = env.MYSQL_DATABASE || 'leckdb';
export const MYSQL_USER: string = env.MYSQL_USER || 'root';
export const MYSQL_PASSWORD: string = env.MYSQL_PASSWORD || 'pwd';
export const MYSQL_HOST: string = env.MYSQL_HOST || '127.0.0.1';
export const SERVER_PORT: number = env.SERVER_PORT ? parseInt(env.SERVER_PORT) : 3000;
