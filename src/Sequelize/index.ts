import {Sequelize} from 'sequelize-typescript';
import {MYSQL_DATABASE, MYSQL_HOST, MYSQL_PASSWORD, MYSQL_USER} from "../config";

export const connect = () => {

    //initialize DB Data for Sequelize
    const sequelize = new Sequelize({
        database: MYSQL_DATABASE,
        dialect: 'mysql',
        host: MYSQL_HOST,
        username: MYSQL_USER,
        password: MYSQL_PASSWORD,
        logging: false,
        modelPaths: [__dirname + './../Models/*.ts'],
        define: {
            charset: 'utf8',
            collate: 'utf8_general_ci',
        },
    });

    //test authentication and connection
    sequelize
        .authenticate()
        .then(() => {
            console.log('Connection has been established successfully.');
        })
        .catch((err: any) => {
            console.error('Unable to connect to the database:', err);
        });

    //create and update DB Schema from custom sequelize Schema in the Project
    sequelize.sync({alter:true})
        .then(() => {
            console.log('Schema generated successfully');
        })
        .catch((err: any) => console.log('Schema not created',err));

    //add Models (Definitions for a Table) to Sequelize
    sequelize.addModels([

    ]);
};
